# AI28 - Machine learning

### Membres :
- GLANDIER Quentin (GI04)
- CHEMANACK Thierry (GI04)

---

### TP :
- TP1: bases librairies python

    | | | |
    |---|---|---|
    | 1 - numpy         | [rendu](AI28_TP1.1.ipynb) | [énoncé](Enoncés/tp_1_1_numpy.pdf) | 
    | 2 - matplotlib    | [rendu](AI28_TP1.2.ipynb) | [énoncé](Enoncés/tp_1_2_matplotlib.pdf) | 
    | 3 - scipy         | [rendu](AI28_TP1.3.ipynb) | [énoncé](Enoncés/tp_1_3_scipy.pdf) | 
    | 4 - pandas        | [rendu](AI28_TP1.4.ipynb) | [énoncé](Enoncés/tp_1_4_pandas.pdf) | 
    | 5 - seaborn       | [rendu](AI28_TP1.5.ipynb) | [énoncé](Enoncés/tp_2_5_seaborn.pdf) | 


- TP2 : Analyse descriptive exploratoire (AED)
    - [ rendu ](TP2/AI28_TP2.ipynb)
    - [ énoncé ](TP2/enonceTP2.pdf)
    
---